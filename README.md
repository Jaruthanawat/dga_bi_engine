# dga-bi-engine

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Docker Build
```
docker-compose build
```

### Docker Run
```
docker run -dit --add-host=qafeedbackservice.openshift.container.data.go.th:164.115.75.24 --add-host=memberservice.openshift.container.data.go.th:164.115.75.24 --name="dga_bi_engine" -p 4000:80 dgabiengine_web
### add host to docker syntax
--add-host=qafeedbackservice.openshift.container.data.go.th:164.115.75.24
```
