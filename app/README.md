# dga_project

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# Enable Tableau CORS policy
tsm stop
tsm configuration set -k vizportal.rest_api.cors.allow_origin -v http://localhost:8080,http://172.21.1.181:4000
tsm configuration set -k vizportal.rest_api.cors.enabled -v true
tsm pending-changes apply

