module.exports = {
    // options...
    devServer: {
        disableHostCheck: true,
        https: false,
        host: '0.0.0.0',
        port: 8080,
        hotOnly: false
    },
    publicPath: './',
}
