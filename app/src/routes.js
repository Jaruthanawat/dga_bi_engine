import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Home from './views/Home.vue'
import DashboardListVisualize from './views/dashboard_list_visualize.vue'
import DashboardFeedback from './views/dashboard_feedback.vue'
import SettingsDataStorePolicy from './views/settings_data_store_policy.vue'
import SettingsPortalLayout from './views/settings_portal_layout.vue'
import SettingsRequiredField from './views/settings_required_field.vue'
import Portal from './views/portal.vue'
import AddFeedback from './views/add_feedback.vue'
import ReplyFeedback from './views/reply_feedback.vue'
import ViewFeedback from './views/view_feedback.vue'
import Tableau from './views/tableau.vue'
import Report from './views/report.vue'
const routerOptions = [
    {
        path: '/',
        component: Home,
        name: 'home'
    },
    {
        path: '/dashboard_list_visualize',
        component: DashboardListVisualize,
        name: 'dashboard_list_visualize'
    },
    {
        path: '/portal',
        component: Portal,
        name: 'portal'
    },
    {
        path: '/report',
        component: Report,
        name: 'report'
    },
    // -----------------------
    {
        path: '/settings_data_store_policy',
        component: SettingsDataStorePolicy,
        name: 'settings_data_store_policy'
    },
    {
        path: '/settings_portal_layout',
        component: SettingsPortalLayout,
        name: 'settings_portal_layout'
    },
    {
        path: '/settings_required_field',
        component: SettingsRequiredField,
        name: 'settings_required_field'
    },
    {
        path: '/tableau',
        component: Tableau,
        name: 'tableau'
    },
    {
        path: '/add_feedback',
        component: AddFeedback,
        name: 'add_feedback'
    },
    {
        path: '/reply_feedback',
        component: ReplyFeedback,
        name: 'reply_feedback'
    },
    {
        path: '/view_feedback',
        component: ViewFeedback,
        name: 'view_feedback'
    },
    {
        path: '/dashboard_feedback',
        component: DashboardFeedback,
        name: 'dashboard_feedback'
    },
]

const routes = routerOptions.map(route => {
    return {
        path: route.path,
        component: route.component,
        name: route.name,
        meta: route.meta
    }
})

const router = new Router({
    mode: 'history',
    routes: [
        ...routes,{
            path: '*',
            redirect: '/'
        }
    ]
})

export default router