export const TABLEAU_CONFIG = {
    name: "admin",
    password: "admin",
    contentUrl: "BI-Portal",
    // IP:"http://164.115.75.62",
    IP: 'https://bi.data.go.th',
    API:"/api/3.5"
}

/* local */
export const IP = 'http://localhost:3000'
export const RedirectURL = 'http://localhost:8080'

/* 181 */
// export const IP = 'http://172.21.1.181:3000'
// export const RedirectURL = 'http://172.21.1.181:4000'

/* CKAN */
// export const IP = 'http://172.21.1.181/bi_api'
// export const RedirectURL = 'http://172.21.1.181/bi_visualize'

/* CKAN */
// export const IP = 'http://164.115.75.68/bi_api'
// export const RedirectURL = 'http://164.115.75.68/bi_visualize'
