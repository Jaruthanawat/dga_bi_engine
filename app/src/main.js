import Vue from 'vue'
import App from './App.vue'
import router from './routes'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import 'sweetalert2/src/sweetalert2.scss'
import VueMeta from 'vue-meta'
import VueResource from 'vue-resource'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'
import '@fortawesome/fontawesome-free/css/all.css'
import 'font-awesome/css/font-awesome.min.css'
import '../src/assets/scss/style.scss'

library.add(fas)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(VueResource)

Vue.use(VueMeta, {
  // optional pluginOptions
  refreshOnceOnNavigation: true
})

Vue.use(Vuetify, {
  iconfont: ['mdi', 'fab', 'md', 'mdiSvg', 'fa', 'fa4'] // 'md' || 'mdi' || 'fa' || 'fa4'
})

Vue.http.headers.common['X-Frame-Options'] = 'ALLOWALL'

window.$ = require('jquery')
window.JQuery = require('jquery')

Vue.config.productionTip = false

new Vue({
  router,
  vuetify: new Vuetify({
    icons: {
      iconfont: 'mdi'
    }
  }),
  render: h => h(App),
}).$mount('#app')
