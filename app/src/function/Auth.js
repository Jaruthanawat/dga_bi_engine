import axios from 'axios'
import {
    IP,
    RedirectURL
} from '../config'

export const Auth = {
    RefreshToken(refresh_token) {
        return new Promise(async (resolve) => {
            axios({
                method: 'post',
                url: IP + '/refresh-token',
                data: {
                    refresh_token: refresh_token
                }
            }).then(response => {
                console.log('refresh token !');
                const data = response.data;
                localStorage.setItem("sso_access_token", data.access_token)
                localStorage.setItem("sso_refresh_token", data.refresh_token)
                resolve(response.data)
            })
        })
    },
    async exchangeCodeForToken(query) {
        return new Promise(async (resolve, reject) => {
            let apiurl = IP + '/exchangeCodeForToken'
            let params = {
                code: query.code,
                redirectUri: encodeURI(`${RedirectURL}/`)
            }
            let res = await axios.post(apiurl, params)
            console.log('exchange code: ', res)
            if (!res.data.error) {
                const sso_token = res.data.access_token
                const refresh_token = res.data.refresh_token
                await localStorage.setItem("sso_access_token", sso_token)
                await localStorage.setItem("sso_refresh_token", refresh_token)
                resolve(true)
            } else {
                console.log('error: ', res.data.error);
            }
        })
    },
    async getSSOUserInformation() {
        return new Promise(async (resolve, reject) => {
            let apiurl = IP + '/getSSOUserInformation'
            let params = {
                ssotoken: localStorage.getItem('sso_access_token')
            }
            let res = await axios.post(apiurl, params)
            console.log('get sso: ', res);
            if (res.data != "" && res.data != null && res.data.errno != "ECONNRESET" && res.data != 'null') {
                console.log('not null.')
                await localStorage.setItem('tableau_user_id', res.data.tableauUserId)
                await localStorage.setItem('ministry', res.data.ministry)
                await localStorage.setItem('department', res.data.department)
                await localStorage.setItem('user_id', res.data.sub)
                await localStorage.setItem('user_name', res.data.username)
                await localStorage.setItem('user_role', JSON.stringify(res.data.assignRole))
                resolve(true)
            } else {
                console.log('null.')
                this.RefreshToken(localStorage.getItem('sso_refresh_token')).then(async () => {
                    const session = localStorage.getItem('session')
                    const session_json = JSON.parse(session);
                    await this.initial(session_json);
                    resolve(true)
                })
            }
        })
    },
    async initial(query) {
        return new Promise(async (resolve, reject) => {
            const sessionState = localStorage.getItem('sessionState')
            const exchange = localStorage.getItem('exchange')

            if (exchange == null) {
                localStorage.setItem('exchange', true);
                await this.exchangeCodeForToken(query).then(async () => {
                    await localStorage.setItem('sessionState', 0);
                    await this.getSSOUserInformation().then(async () => {
                        console.log('new: login success !')
                        resolve('new: login success!')
                    })
                })
            }else{
                if(sessionState == 0){
                    await this.getSSOUserInformation().then(async () => {
                        console.log('old: login success !')
                        resolve('old: login success!')
                    })
                }else{
                    await this.exchangeCodeForToken(query).then(async () => {
                        await localStorage.setItem('sessionState', 0);
                        await this.getSSOUserInformation().then(async () => {
                            console.log('old: login success !')
                            resolve('old: login success!')
                        })
                    })
                }
            }
        }, this)
    },
}