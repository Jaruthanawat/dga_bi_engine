# FROM ebiven/vue-cli
FROM node:11-alpine

# RUN rm -rf node_modules package-lock.json && npm install -g http-server && npm install -g --save-dev webpack-dev-server
RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY /app/package.json /usr/src/app

RUN npm install

COPY /app /usr/src/app

EXPOSE 80

# RUN npm run serve

#run serve
CMD ["npm","run","serve","--","--port","80"]
